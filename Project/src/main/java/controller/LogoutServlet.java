package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LogoutServlet
 */
@WebServlet("/LogoutServlet")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogoutServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	HttpSession session = request.getSession();
		if("log".equals(session.getAttribute("log")) == false) 
			response.sendRedirect("LoginServlet");
		else {

		// ログイン時に保存したセッション内のユーザ情報を削除
		session.removeAttribute("userInfo");
		session.removeAttribute("log");
		// ログインのサーブレットにリダイレクト
		response.sendRedirect("LoginServlet");
		}
	}
}
