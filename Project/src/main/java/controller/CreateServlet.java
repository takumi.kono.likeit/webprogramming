package controller;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;

/**
 * Servlet implementation class CreateServlet
 */
@WebServlet("/CreateServlet")
public class CreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CreateServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		if ("log".equals(session.getAttribute("log")) == false)
			response.sendRedirect("LoginServlet");
		else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
			dispatcher.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		String check = request.getParameter("check");
		String name = request.getParameter("name");
		String bd = request.getParameter("bd");
		boolean a = false;
		
		String source = pw;
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(source.getBytes());
			byte[] hashBytes = md.digest();
			int[] hashInts = new int[hashBytes.length];
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < hashBytes.length; i++) {
				hashInts[i] = (int) hashBytes[i] & 0xff;
				if (hashInts[i] <= 15) {
					sb.append("0");
				}
				sb.append(Integer.toHexString(hashInts[i]));
			}
			String str = sb.toString();

			List<String> fid = UserDao.findId();
			for (String i : fid) {
				if (i.equals(id))
					a = true;
			}
			if(id == "" || pw == "" || check == "" || name == "" || bd == "" || a) {
				request.setAttribute("id", id);
				request.setAttribute("name", name);
				request.setAttribute("bd", bd);
				request.setAttribute("errMsg", "入力された情報は正しくありません");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
				dispatcher.forward(request, response);
				return;
			}
			else if (pw.equals(check))
				UserDao.createUser(id, str, name, bd);
			else {
				request.setAttribute("errMsg", "入力された情報は正しくありません");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
				dispatcher.forward(request, response);
				return;
			}

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		response.sendRedirect("UserListServlet");

	}

}
