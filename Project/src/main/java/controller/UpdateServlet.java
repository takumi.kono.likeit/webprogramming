package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;

@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UpdateServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if("log".equals(session.getAttribute("log")) == false) 
			response.sendRedirect("LoginServlet");
		else {
		request.setCharacterEncoding("UTF-8");
		
		String id = request.getParameter("id");
		model.User user = dao.UserDao.findUser(id);
		request.setAttribute("userInfo", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		String check = request.getParameter("check");
		String name = request.getParameter("name");
		String bd = request.getParameter("bd");
		if(name=="" || bd==""){
			request.setAttribute("name", name);
			request.setAttribute("bd", bd);
			request.setAttribute("errMsg", "入力された情報は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response); }
		else if(pw == "" && check == "")
		UserDao.IfupdateUser(name, bd, id);
		else if(pw.equals(check))
		UserDao.updateUser(pw, name, bd,id);
		else{
			request.setAttribute("name", name);
			request.setAttribute("bd", bd);
			request.setAttribute("errMsg", "入力された情報は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response); }
		 
		response.sendRedirect("UserListServlet");
	}

}
