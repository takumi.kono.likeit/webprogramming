package dao;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import model.User;

public class UserDao {

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * 
	 * @param loginId
	 * @param password
	 * @return
	 */
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			String source = password;
			MessageDigest md;
			try {
				md = MessageDigest.getInstance("MD5");
				md.update(source.getBytes());
				byte[] hashBytes = md.digest();
				int[] hashInts = new int[hashBytes.length];
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < hashBytes.length; i++) {
					hashInts[i] = (int) hashBytes[i] & 0xff;
					if (hashInts[i] <= 15) {
						sb.append("0");
					}
					sb.append(Integer.toHexString(hashInts[i]));
				}
				String str = sb.toString();

				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, loginId);
				pStmt.setString(2, str);
				ResultSet rs = pStmt.executeQuery();

				// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
				if (!rs.next()) {
					return null;
				}

				// 必要なデータのみインスタンスのフィールドに追加
				String loginIdData = rs.getString("login_id");
				String nameData = rs.getString("name");
				int id = rs.getInt("id");
				return new User(loginIdData, nameData, id);

			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	/**
	 * 全てのユーザ情報を取得する
	 * 
	 * @return
	 */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id >1";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				Date createDate = rs.getDate("create_date");
				Timestamp updateDate = rs.getTimestamp("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}
	
	public static List<User> searchUser(String a, String b, String c, String d) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = DBManager.getConnection();
			
			StringBuilder sb = new StringBuilder();
			sb.append("select * from user where ");
			boolean check = true;
			
			if(a != "")
				sb.append("login_id = '" + a + "' && ");
			if(b != "")
				sb.append("name like '%" + b + "%' && ");
			if(c == "" && d == "");
			else if(c != "" && d != "") {
				sb.append("birth_date >= '" + c + "' && birth_date <= '" + d + "'");
				check = false;}
			else if(c != "") {
				sb.append("birth_date >= '" + c + "' && birth_date <= 99999999" );
				check = false;}
			else {
				sb.append("birth_date >= 00010101 && birth_date <= '" + d +"'");
				if(a == "" && b == "")
				check = false;
				}
			
			if(check) {
				int ch = sb.lastIndexOf("&&");
				sb.delete(ch, ch+3);}
			
			String sql = sb.toString();

			Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				Date createDate = rs.getDate("create_date");
				Timestamp updateDate = rs.getTimestamp("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}
	
	
	public static List<String> findId() {
		Connection conn = null;
		List<String> IdList = new ArrayList<String>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT login_id FROM user";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				String id = rs.getString("login_id");
				IdList.add(id);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return IdList;
	}

	public static void createUser(String id, String pw, String name, String bd) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "insert into user values(null, ? , ? , ? , ?, now(), now())";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			pStmt.setString(2, name);
			pStmt.setString(3, bd);
			pStmt.setString(4, pw);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static User findUser(String userId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user where id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			Timestamp createDate = rs.getTimestamp("create_date");
			Timestamp updateDate = rs.getTimestamp("update_date");
			return new User(id, loginId, name, birthDate, password, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public static void updateUser(String pw, String name, String bd, String id) {
		Connection conn = null;
		try {
			String source = pw;
			MessageDigest md;
			try {
				md = MessageDigest.getInstance("MD5");
				md.update(source.getBytes());
				byte[] hashBytes = md.digest();
				int[] hashInts = new int[hashBytes.length];
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < hashBytes.length; i++) {
					hashInts[i] = (int) hashBytes[i] & 0xff;
					if (hashInts[i] <= 15) {
						sb.append("0");
					}
					sb.append(Integer.toHexString(hashInts[i]));
				}
				String str = sb.toString();
				conn = DBManager.getConnection();

				String sql = "update user set name = ?, birth_date = ? , password = ? , update_date = now() where id = ?";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, name);
				pStmt.setString(2, bd);
				pStmt.setString(3, str);
				pStmt.setString(4, id);
				pStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
			}

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void IfupdateUser(String name, String bd, String id) {
		Connection conn = null;
		try {
				conn = DBManager.getConnection();

				String sql = "update user set name = ?, birth_date = ? , update_date = now() where id = ?";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, name);
				pStmt.setString(2, bd);
				pStmt.setString(3, id);
				pStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void deleteUser(String userId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "delete FROM user where id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userId);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
