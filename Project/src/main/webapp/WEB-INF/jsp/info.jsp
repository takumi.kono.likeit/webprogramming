<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utd-8">
    <title>ユーザ情報参照</title>
    <link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
    
     <style>
	a {text-decoration:underline;}	
	</style>

</head>

<body>
    <div class="row">
        <div class="col-sm-12">
        <div style="background-color:#808080;" >
       
        <p style="text-align: right">
            <font color="white">${sessionScope.userInfo.name}さん　　　</font>
            <a href="LoginServlet"><font color="red">ログアウト</font></a>
        </p>
        </div>
        </div>
        </div>
    <div style="text-align:center;">
    <br><h1>ユーザ情報詳細参照</h1><br>
    </div>
        <div class="row">
            <div class="col-sm-6">
            <h3>
                ログインID　
            </h3>
        </div>
            <div class="col-sm-6">
                <p>
                  ${userInfo.loginId}  
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
            <h3>
                ユーザ名
            </h3>
        </div>
            <div class="col-sm-6">
                <p>
                    ${userInfo.name}
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
            <h3>
                生年月日
            </h3>
        </div>
            <div class="col-sm-6">
                <p>
                <fmt:formatDate value="${userInfo.birthDate}" pattern="yyyy年MM月dd日" />
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
            <h3>
                登録日時
            </h3>
        </div>
            <div class="col-sm-6">
                <p>
                   <fmt:formatDate value="${userInfo.createDate}" pattern="yyyy年MM月dd日 hh:mm" />
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
            <h3>
                更新日時
            </h3>
        </div>
            <div class="col-sm-6">
                <p>
                    <fmt:formatDate value="${userInfo.updateDate}" pattern="yyyy年MM月dd日 hh:mm" />
                </p>
            </div>
        </div>
       
        <br><br>
        <a href="UserListServlet">戻る</a>

</body>

</html>