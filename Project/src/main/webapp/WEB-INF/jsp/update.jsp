<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utd-8">
<title>ユーザ情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

<style>
a {
	text-decoration: underline;
}
</style>


</head>

<body>

	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>

	<div class="row">
		<div class="col-sm-12">
			<div style="background-color: #808080;">

				<p style="text-align: right">
					<font color="white">${sessionScope.userInfo.name}さん　　　 </font> <a
						href="LogoutServlet"><font color="red">ログアウト</font></a>
				</p>
			</div>
		</div>
	</div>
	<div style="text-align: center;">
		<br>
		<h1>ユーザ情報更新</h1>
		<br>
	</div>
	<br>

	<form action="UpdateServlet?id=${userInfo.id}" method="post">
		<div class="row">
			<div class="col-sm-6">
				<h3>ログインID</h3>
			</div>
			<div class="col-sm-6">
				<p>${userInfo.loginId}</p>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-6">
				<h3>パスワード</h3>
			</div>
			<div class="col-sm-6">
				<input type="text" name="pw" style="width: 200px;" autofocus>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-6">
				<h3>パスワード（確認）</h3>
			</div>
			<div class="col-sm-6">
				<input type="text" name="check" style="width: 200px;">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-6">
				<h3>ユーザ名</h3>
			</div>
			<div class="col-sm-6">
				<c:choose>
					<c:when test="${name != null}">
						<input type="text" name="name" value="${name}"
							style="width: 200px;" >
					</c:when>
					<c:otherwise>
						<input type="text" name="name" value="${userInfo.name}"
							style="width: 200px;" >
					</c:otherwise>
				</c:choose>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-6">
				<h3>生年月日</h3>
			</div>
			<div class="col-sm-6">
				<c:choose>
					<c:when test="${bd != null}">
						<input type="date" name="bd" value="${bd}"
							style="width: 200px;" >
					</c:when>
					<c:otherwise>
						<input type="date" name="bd" value="${userInfo.birthDate}"
							style="width: 200px;" >
					</c:otherwise>
				</c:choose>
			</div>
		</div>
		<br>
		<div style="text-align: center">
			<input type="submit" value="  　　更新  　　">
		</div>
		<br>
	</form>

	<a href="UserListServlet">戻る</a>


</body>

</html>