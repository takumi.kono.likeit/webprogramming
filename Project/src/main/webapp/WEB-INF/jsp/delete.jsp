<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utd-8">
    <title>ユーザ削除</title>
    <link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">

	<style>
	a {text-decoration:underline;}
	</style>
</head>

<body>
    <div class="row">
        <div class="col-sm-12">
        <div style="background-color:#808080;" >
       
        <p style="text-align: right">
            <font color="white">${userInfo.name}さん　　　</font>
            <a href="login.html"><font color="red">ログアウト</font></a>
        </p>
        </div>
        </div>
        </div>
    <div style="text-align:center;">
    <br><h1>ユーザ削除確認</h1><br>
    
    <br>
    <h2>
        ログインID：${userInfo.loginId}<br>
        を本当に削除してよろしいでしょうか？
    </h2>
    <br><br><br>
    </div>
    
    <table align="center" cellSpacing=100>
    <tr>
    <td>
    <form action="UserListServlet" method="get">
                <input type="submit" value="　キャンセル　">
    </form>
    </td>
    <td>
    <form action="DeleteServlet?id=${userInfo.id}" method="post">
                <input type="submit" value="　　OK　　">
    </form>
    </td>
    </tr>
	</table>    
</body>

</html>