<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.io.*,java.util.*" %>
<%@ page import="javax.servlet.*,java.text.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utd-8">
<title>ユーザ一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

<style>
a {
	text-decoration: underline;
}

#center {
	margin: auto;
}
</style>

</head>

<body>
	<div class="row">
		<div class="col-sm-12">
			<div style="background-color: #808080;">

				<p style="text-align: right">
					<font color="white">${userInfo.name}さん　　　 </font> <a
						href="LogoutServlet"><font color="red">ログアウト</font></a>
				</p>
			</div>
		</div>
	</div>
	<div style="text-align: center;">
		<br>
		<h1>ユーザ一覧</h1>
		<fmt:formatDate value="${user.birthDate}" pattern="yyyy年MM月dd日" />
		<br>
	</div>
	<p style="text-align: right">
		<a href="CreateServlet">新規登録</a>
	</p>

	<div style="text-align:center;">
    <form action="UserListServlet" method="post">
        <div class="row">
        <div class="col-sm-12">
        	<c:choose>
					<c:when test="${id != null}">
					<h3>ログインID　
          			<input type="text" name="id" value="${id}"style="width:400px;">
           			</h3>
					</c:when>
					<c:otherwise>
						<h3>ログインID　
          			<input type="text" name="id" style="width:400px;">
           			</h3>
					</c:otherwise>
			</c:choose>
        </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
            <c:choose>
					<c:when test="${name != null}">
					<h3>ユーザ名　　
            		<input type="text" name="name" value="${name}"style="width:400px;">
           			</h3>
					</c:when>
					<c:otherwise>
					<h3>ユーザ名　　
          			 <input type="text" name="name" style="width:400px;">
           			 </h3>
					</c:otherwise>
			</c:choose>
            </div>
        </div>
        <div class="row">
        <div class="col-sm-12">
        <c:choose>
					<c:when test="${start != null}">
					<h3>生年月日　　
            		<input type="date" name="start" value="${start}"style="width:150px;font-size:18px">
            		 　～　
              		 <input type="date" name="end" value="${end}" style="width:150px;font-size:18px">
           			</h3>
					</c:when>
					<c:otherwise>
					<h3>生年月日　　
          			 <input type="date" name="start" value="年/月/日" style="width:150px;font-size:18px">
              		  　～　
              		  <input type="date" name="end" value="年/月/日" style="width:150px;font-size:18px">
           			 </h3>
					</c:otherwise>
			</c:choose>
                </div>
                </div>
            <div style="text-align: right">
            <input type="submit" value="  　　検索  　　">
            </div>


		</form>
	</div>
	<hr>

	<table id=center border="1" width="40%">
		<tr align="center" bgcolor="f5f5f5">
			<th>ログインID</th>
			<th>ユーザ名</th>
			<th>生年月日</th>
			<th></th>
		</tr>
		<c:choose>
			<c:when test="${searchUser != null}">
				<<c:forEach var="user" items="${searchUser}">
			<tr>
				<td>${user.loginId}</td>
				<td>${user.name}</td>
				<td><fmt:formatDate value="${user.birthDate}" pattern="yyyy年MM月dd日" /></td>
				<td><a class="btn btn-primary" href="InfoServlet?id=${user.id}">詳細</a>
					<c:if test="${userInfo.id==user.id || userInfo.id==1}">
						<a class="btn btn-success" href="UpdateServlet?id=${user.id}">更新</a>
					</c:if> <c:if test="${userInfo.id==1}">
						<a class="btn btn-danger" href="DeleteServlet?id=${user.id}">削除</a>
					</c:if></td>
			</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<c:forEach var="user" items="${userList}">
			<tr>
				<td>${user.loginId}</td>
				<td>${user.name}</td>
				<td><fmt:formatDate value="${user.birthDate}" pattern="yyyy年MM月dd日" /></td>
				<td><a class="btn btn-primary" href="InfoServlet?id=${user.id}">詳細</a>
					<c:if test="${userInfo.id==user.id || userInfo.id==1}">
						<a class="btn btn-success" href="UpdateServlet?id=${user.id}">更新</a>
					</c:if> <c:if test="${userInfo.id==1}">
						<a class="btn btn-danger" href="DeleteServlet?id=${user.id}">削除</a>
					</c:if></td>
			</tr>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</table>
</body>

</html>